Contents
========

1.  [Description](#description)
2.  [License](#license)
3.  [Requirements](#requirements)
4.  [Installation](#installation)
5.  [Instructions](#instructions)
6.  [Contact](#contact)
7.  [Contributing](#contributing)

Description
===========

PyColorTools is a python library that implements some utilities functions for color conversion from RGB to YCbCr (YUV) and back. This library also implements functions to join 3 independent grayscale channels to a single color image. The split function, used to separate the 3-dimension color image to 3 gray images with 1 dimension each, is also implemented.

License
=======

PyColorTools is released under [GNU GPL version
2.](http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt)


Requirements
============

PyColorTools was tested with [Python 2.7](https://www.python.org/download/releases/2.7/) and [Python 3.4.0](https://www.python.org/download/releases/3.4.0/), [Scipy 0.10.0](http://www.scipy.org/), [Numpy 1.9.0](http://www.numpy.org/), and [Cython 0.21.1](http://cython.org/).

Installation
===========

1. Clone the last version
> `# hg clone https://bitbucket.org/kuraiev/pycolortools`
2. Go to the directory where 'setup.py' os located
> `cd pycolortools`
3. Install using distributils
> `# sudo python setup.py install`

Instructions
============

The repository 'test' contains some examples of how to use the functions.

Contact
=======

Please send all comments, questions, reports and suggestions (especially if you would like to contribute) to **sawp@sawp.com.br**

Contributing
============

If you would like to contribute with new algorithms, increment the code performance, documentation or another kind of modifications, please contact me. The only requirements are: keep the code compatible with
PEP8 standardization and licensed by GPLv2.

References
==========

[1]  http://www.equasys.de/colorconversion.html

[2] ITU-R, Rec. BT. 601-4. **Encoding parameters of digital television for studios**, 2001.

[3] ITU, ITURBT. **709-5 Parameter values for the HDTV standards for production and international programme exchange.** (2002).
           
[4] Malvar, Henrique, and Gary Sullivan. **YCoCg-R: A color space with RGB reversibility and low dynamic range.** ISO/IEC JTC1/SC29/WG11 and ITU (2003).