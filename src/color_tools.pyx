# encoding: utf-8
# cython: profile=True
# filename: color_tools.pyx

from __future__ import division
import itertools
import numpy as np

cimport numpy as np

ctypedef np.uint8_t uint8

cpdef __rgb2ycbcr_ntsc(float r, float g, float b):
    """
    Get Y, Cb, and Cr values for a single pixel from the R, G, and B values.

    Using this function the YUV color format is used only for analog PAL or
    analog NTSC video.

    Parameters
    ----------
    R: float
        Red value in range [0..1].
    G: float
        Green value in range [0..1].
    B: float
        Blue value in range [0..1].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [0..1].
    Cb: float
        Chrominance value of single pixel in range [-0.436..+0.436].
    Cr: float
        Chrominance value of single pixel in range [-0.615..+0.615].

    """
    cdef float y = <float> (0.299 * r + 0.587 * g + 0.114 * b)
    cdef float cb = <float> (-0.147 * r - 0.289 * g + 0.436 * b)
    cdef float cr = <float> (0.615 * r - 0.515 * g - 0.100 * b)
    return y, cb, cr

cpdef __ycbcr2rgb_ntsc(float  y, float cb, float cr):
    """
    Convert a single pixel from YCbCr (PAL/NTSC) to RGB in range [0..1]

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [0..1].
    Cb: float
        Chrominance value of single pixel in range [-0.436..+0.436].
    Cr: float
        Chrominance value of single pixel in range [-0.615..+0.615].

    Returns
    -------
    R: float
        Red value in range [0..1].
    G: float
        Green value in range [0..1].
    B: float
        Blue value in range [0..1].

    """
    cdef float r = <float> (1.000 * y + 0.000 * cb + 1.140 * cr)
    cdef float g = <float> (1.000 * y - 0.395 * cb - 0.581 * cr)
    cdef float b = <float> (1.000 * y + 2.032 * cb + 0.000 * cr)
    return r, g, b

cpdef __rgb2ycbcr_sdtv(float r, float g, float b):
    """
    Convert a single pixel from RGB 8:8:8 to YCbCr (according to ITU-R BT.601)

    Parameters
    ----------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [16..235].
    Cb: float
        Chrominance value of single pixel in range [16..240].
    Cr: float
        Chrominance value of single pixel in range [16..240].

    References
    ----------
    .. [1] ITU-R, Rec. BT. 601-4. *Encoding parameters of digital television
           for studios*, 2001.

    """
    cdef float y = <float> (0.257 * r + 0.504 * g + 0.098 * b)
    cdef float cb = <float> (-0.148 * r - 0.291 * g + 0.439 * b)
    cdef float cr = <float> (0.439 * r - 0.368 * g - 0.071 * b)
    return y + 16, cb + 128, cr + 128

cpdef __ycbcr2rgb_sdtv(float y_u, float cb_u, float cr_u):
    """
    Convert a single pixel from YCbCr to RGB 8:8:8 (according to ITU-R BT.601)

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [16..235].
    Cb: float
        Chrominance value of single pixel in range [16..240].
    Cr: float
        Chrominance value of single pixel in range [16..240].

    Returns
    -------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    References
    ----------
    .. [1] ITU-R, Rec. BT. 601-4. *Encoding parameters of digital television
           for studios*, 2001.

    """
    cdef float y = y_u - 16
    cdef float cb = cb_u - 128
    cdef float cr = cr_u - 128
    cdef float r = <float> (1.164 * y + 0.000 * cb + 1.596 * cr)
    cdef float g = <float> (1.164 * y - 0.392 * cb - 0.813 * cr)
    cdef float b = <float> (1.164 * y + 2.017 * cb + 0.000 * cr)
    return r, g, b

cpdef __rgb2ycbcr_hdtv(float r, float g, float b):
    """
    Convert a single pixel from RGB 8:8:8 to YCbCr (according to ITU-R BT.709)

    Parameters
    ----------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [16..235].
    Cb: float
        Chrominance value of single pixel in range [16..240].
    Cr: float
        Chrominance value of single pixel in range [16..240].

    References
    ----------
    .. [1] ITU, ITURBT. *709-5 Parameter values for the HDTV standards for
           production and international programme exchange.* (2002).
    .. [2] Malvar, Henrique, and Gary Sullivan. *YCoCg-R: A color space with
           RGB reversibility and low dynamic range.* ISO/IEC
           JTC1/SC29/WG11 and ITU (2003).

    """
    cdef float y = <float> (0.183 * r + 0.614 * g + 0.062 * b)
    cdef float cb = <float> (-0.101 * r - 0.339 * g + 0.439 * b)
    cdef float cr = <float> (0.439 * r - 0.399 * g - 0.040 * b)
    return y + 16, cb + 128, cr + 128

cpdef __ycbcr2rgb_hdtv(float y_u, float cb_u, float cr_u):
    """
    Convert a single pixel from YCbCr to RGB 8:8:8 (according to ITU-R BT.709)

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [16..235].
    Cb: float
        Chrominance value of single pixel in range [16..240].
    Cr: float
        Chrominance value of single pixel in range [16..240].

    Returns
    -------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    References
    ----------
    .. [1] ITU, ITURBT. *709-5 Parameter values for the HDTV standards for
           production and international programme exchange.* (2002).
    .. [2] Malvar, Henrique, and Gary Sullivan. *YCoCg-R: A color space with
           RGB reversibility and low dynamic range.* ISO/IEC
           JTC1/SC29/WG11 and ITU (2003).

    """
    cdef float y = y_u - 16
    cdef float cb = cb_u - 128
    cdef float cr = cr_u - 128
    cdef float r = <float> (1.164 * y + 0.000 * cb + 1.793 * cr)
    cdef float g = <float> (1.164 * y - 0.213 * cb - 0.533 * cr)
    cdef float b = <float> (1.164 * y + 2.112 * cb + 0.000 * cr)
    return r, g, b

cpdef __rgb2ycbcr_fullrange(float r, float g, float b):
    """
    Convert a single pixel from RGB 8:8:8 to YCbCr 8:8:8 (both 0 to 255).

    This function is useful for computer based applications using RGB and YCbCr
    color formats, in many cases the complete possible range of 8 bit is used,
    without providing a footroom or headroom. Typically, this full-range color
    format is used for JPEG images.

    Parameters
    ----------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [0..255].
    Cb: float
        Chrominance value of single pixel in range [0..255].
    Cr: float
        Chrominance value of single pixel in range [0..255].

    """
    cdef float y = <float> (0.299 * r + 0.587 * g + 0.114 * b)
    cdef float cb = <float> (-0.169 * r - 0.331 * g + 0.500 * b)
    cdef float cr = <float> (0.500 * r - 0.419 * g - 0.081 * b)
    return y, cb + 128, cr + 128

cpdef __ycbcr2rgb_fullrange(float y, float cb_u, float cr_u):
    """
    Convert a single pixel from YCbCr 8:8:8 to RGB 8:8:8 (both 0 to 255).

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [0..255].
    Cb: float
        Chrominance value of single pixel in range [0..255].
    Cr: float
        Chrominance value of single pixel in range [0..255].

    Returns
    -------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    See Also
    --------
    __rgb2ycbcr_fullrange

    """
    cdef float cb = cb_u - 128
    cdef float cr = cr_u - 128
    cdef float r = <float> (1.000 * y + 0.000 * cb + 1.400 * cr)
    cdef float g = <float> (1.000 * y - 0.343 * cb - 0.711 * cr)
    cdef float b = <float> (1.000 * y + 1.765 * cb + 0.000 * cr)
    return r, g, b

cpdef __rgb2ypbpr_sdtv(float r, float g, float b):
    """
    Convert a single pixel from RGB 8:8:8 to YPbPr for SDTV.

    Parameters
    ----------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [0..255].
    Pb: float
        Chrominance value of single pixel in range [-127.5..+127.5].
    Pr: float
        Chrominance value of single pixel in range [-127.5..+127.5].

    """
    cdef float y = <float> (0.299 * r + 0.587 * g + 0.114 * b)
    cdef float cb = <float> (-0.169 * r - 0.331 * g + 0.500 * b)
    cdef float cr = <float> (0.500 * r - 0.419 * g - 0.081 * b)
    return y, cb, cr

cpdef __ypbpr2rgb_sdtv(float y, float cb, float cr):
    """
    Convert a single pixel from YPbPr to RGB 8:8:8 for SDTV.

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [0..255].
    Pb: float
        Chrominance value of single pixel in range [-127.5..+127.5].
    Pr: float
        Chrominance value of single pixel in range [-127.5..+127.5].

    Returns
    -------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    """
    cdef float r = <float> (1.000 * y + 0.000 * cb + 1.402 * cr)
    cdef float g = <float> (1.000 * y - 0.344 * cb - 0.714 * cr)
    cdef float b = <float> (1.000 * y + 1.772 * cb + 0.000 * cr)
    return r, g, b

cpdef __rgb2ypbpr_hdtv(float r, float g, float b):
    """
    Convert a single pixel from RGB 8:8:8 to YPbPr for HDTV.

    Parameters
    ----------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [0..255].
    Pb: float
        Chrominance value of single pixel in range [-127.5..+127.5].
    Pr: float
        Chrominance value of single pixel in range [-127.5..+127.5].

    """
    cdef float y = <float> (0.213 * r + 0.715 * g + 0.072 * b)
    cdef float pb = <float> (-0.115 * r - 0.385 * g + 0.500 * b)
    cdef float pr = <float> (0.500 * r - 0.454 * g - 0.046 * b)
    return y, pb, pr

cpdef __ypbpr2rgb_hdtv(float y, float pb, float pr):
    """
    Convert a single pixel from YPbPr to RGB 8:8:8 for HDTV.

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [0..255].
    Pb: float
        Chrominance value of single pixel in range [-127.5..+127.5].
    Pr: float
        Chrominance value of single pixel in range [-127.5..+127.5].

    Returns
    -------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    """
    cdef float r = <float> (1.000 * y + 0.000 * pb + 1.575 * pr)
    cdef float g = <float> (1.000 * y - 0.187 * pb - 0.468 * pr)
    cdef float b = <float> (1.000 * y + 1.856 * pb + 0.000 * pr)
    return r, g, b

cpdef __rgb2yuv(float r, float g, float b):
    """
    Convert a single pixel from RGB to YUV.

    Parameters
    ----------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    Returns
    -------
    Y: float
        Luminance value of single pixel in range [0..255].
    U: float
        Chrominance value of single pixel in range [-127.5..+127.5].
    V: float
        Chrominance value of single pixel in range [-127.5..+127.5].

    """
    cdef float y = <float> (0.299 * r + 0.587 * g + 0.114 * b)
    cdef float u = <float> (0.492 * (b - y))
    cdef float v = <float> (0.877 * (r - y))
    return y, u, v

cpdef __yuv2rgb(float y, float u, float v):
    """
    Convert a single pixel from YUV to RGB.

    Parameters
    ----------
    Y: float
        Luminance value of single pixel in range [0..255].
    U: float
        Chrominance value of single pixel in range [-127.5..+127.5].
    V: float
        Chrominance value of single pixel in range [-127.5..+127.5].

    Returns
    -------
    R: float
        Red value in range [0..255].
    G: float
        Green value in range [0..255].
    B: float
        Blue value in range [0..255].

    """
    cdef float r = <float> (y + 1.140 * v)
    cdef float g = <float> (y - 0.395 * u - 0.581 * v)
    cdef float b = <float> (y + 2.032 * u)
    return r, g, b

cpdef __convert_channel(np.ndarray domain, fun):
    """
    Convert a image from a color space to another using a function to map
    the single-pixel color conversion operation.

    Parameters
    ----------
    domain: numpy.ndarray
        Image with 3-channels in original space.
    fun: lambda
        Function which maps the single-pixel conversion operation.

    Returns
    -------
    converted: numpy.ndarray
        Image with 3-channels in converted space (after applying 'fun' in
        each pixel.

    """
    cdef int h = domain.shape[0]
    cdef int w = domain.shape[1]
    cdef int c = domain.shape[2]
    cdef np.ndarray c1, c2, c3
    cdef np.ndarray converted = np.zeros((h, w, c))
    c1, c2, c3 = split_channels(domain)
    for i, j in itertools.product(range(h), range(w)):
        converted[i, j, :] = fun(c1[i, j], c2[i, j], c3[i, j])
    return converted

cpdef join_channels(c1, c2, c3):
    """
    Get three matrices and convert into a three dimension matrix.

    Parameters
    ----------
    c1, c2, c3: numpy.ndarray
        Matrices of each channel.

    Returns
    -------
    b: numpy.ndarray
        Three-dimensional color matrix.

    See Also
    --------
    split_channels
    """
    w, h = c1.shape
    return np.dstack((c1, c2, c3))

cpdef split_channels(channels):
    """
    Split a single three-dimensional matrix to three matrices.

    Parameters
    ----------
    channels: numpy.ndarray
        Three-dimensional color matrix.

    Returns
    -------
    c1, c2, c3: numpy.ndarray
        Matrices of each channel.

    See Also
    --------
    join_channels
    """
    w, h, c = channels.shape
    if c == 3:
        return channels[..., 0], channels[..., 1], channels[..., 2]
    else:
        return channels

cpdef rgb2yuv(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2yuv)

cpdef yuv2rgb(np.ndarray yuv):
    return __convert_channel(yuv, __yuv2rgb)

cpdef rgb2ycbcr_fullrange(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2ycbcr_fullrange)

cpdef ycbcr2rgb_fullrange(np.ndarray ycbcr):
    return __convert_channel(ycbcr, __ycbcr2rgb_fullrange)

cpdef rgb2ycbcr_ntsc(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2ycbcr_ntsc)

cpdef ycbcr2rgb_ntsc(np.ndarray ycbcr):
    return __convert_channel(ycbcr, __ycbcr2rgb_ntsc)

cpdef rgb2ycbcr_sdtv(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2ycbcr_sdtv)

cpdef ycbcr2rgb_sdtv(np.ndarray ycbcr):
    return __convert_channel(ycbcr, __ycbcr2rgb_sdtv)

cpdef rgb2ycbcr_hdtv(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2ycbcr_hdtv)

cpdef ycbcr2rgb_hdtv(np.ndarray ycbcr):
    return __convert_channel(ycbcr, __ycbcr2rgb_hdtv)

cpdef rgb2ypbpr_sdtv(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2ypbpr_sdtv)

cpdef ypbpr2rgb_sdtv(np.ndarray ypbpr):
    return __convert_channel(ypbpr, __ypbpr2rgb_sdtv)

cpdef rgb2ypbpr_hdtv(np.ndarray rgb):
    return __convert_channel(rgb, __rgb2ypbpr_hdtv)

cpdef ypbpr2rgb_hdtv(np.ndarray ypbpr):
    return __convert_channel(ypbpr, __ypbpr2rgb_hdtv)

