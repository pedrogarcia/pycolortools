from scipy.misc import *
import color_tools
import skimage

a = imread('baboon.jpg')

ycbcr_fullrange = color_tools.rgb2ycbcr_fullrange(a)
rgb_fullrange = color_tools.ycbcr2rgb_fullrange(ycbcr_fullrange)

ycbcr_ntsc = color_tools.rgb2ycbcr_fullrange(skimage.img_as_float(a))
rgb_ntsc = color_tools.ycbcr2rgb_fullrange(ycbcr_ntsc)

ycbcr_sdtv = color_tools.rgb2ycbcr_sdtv(a)
rgb_sdtv = color_tools.ycbcr2rgb_sdtv(ycbcr_sdtv)

ycbcr_hdtv = color_tools.rgb2ycbcr_hdtv(a)
rgb_hdtv = color_tools.ycbcr2rgb_hdtv(ycbcr_hdtv)

ypbpr_sdtv = color_tools.rgb2ypbpr_sdtv(a)
rgb_hdtv2 = color_tools.ypbpr2rgb_sdtv(ypbpr_sdtv)

ypbpr_hdtv = color_tools.rgb2ypbpr_hdtv(a)
rgb_sdtv2 = color_tools.ypbpr2rgb_hdtv(ypbpr_hdtv)

yuv = color_tools.rgb2yuv(a)
rgb_from_yuv = color_tools.yuv2rgb(yuv)

imsave('ycbcr_fullrange.png', ycbcr_fullrange)
imsave('rgb_fullrange.png', rgb_fullrange)
imsave('ycbcr_sdtv.png', ycbcr_sdtv)
imsave('rgb_sdtv.png', rgb_sdtv)
imsave('ycbcr_hdtv.png', ycbcr_hdtv)
imsave('rgb_hdtv.png', rgb_hdtv)

imsave('ypbpr_sdtv.png', ypbpr_sdtv)
imsave('rgb_sdtv2.png', rgb_sdtv2)
imsave('ypbpr_hdtv.png', ypbpr_hdtv)
imsave('rgb_hdtv2.png', rgb_hdtv2)

imsave('ycbcr_ntsc.png', ycbcr_ntsc)
imsave('rgb_ntsc.png', 255 * rgb_ntsc)
imsave('yuv.png', yuv)
imsave('rgb_from_yuv.png', rgb_from_yuv)